<?php
/*
Plugin Name: Configurador de Pistas
Plugin URI: https://www.aloewebs.com
Description: Permite configurar una pista de Pádel y compartirla. Utilizar el 'shortcode' [configurador_pistas {pista=N}]
Version: 1.0.30
Author: Juan Jesús Acevedo
License: GPLv2
*/

if( !function_exists( 'add_action' ) ){
	die( "Hi there! I'm just a plugin, not much I can do when called directly." );
}

// SETUP
define( 'CPISTAS_PLUGIN_URL', __FILE__ );

// INCLUDES
include( 'includes/activate.php');
include( 'includes/enqueue.php' );
include( 'includes/configurador-sc.php' );
include( 'includes/ajax.php' );
include( 'includes/admin/menus.php');
include( 'includes/admin/options-page.php');
include( 'includes/process/save-options.php');

// HOOKS
register_activation_hook(__FILE__, 'jjcp_activate_plgin');
add_action( 'wp_enqueue_scripts', 'jjcp_register_plugin_styles' );
add_action( 'wp_ajax_jjcp_configurar', 'jjcp_ajax_configurar' );
add_action( 'wp_ajax_nopriv_jjcp_configurar', 'jjcp_ajax_configurar' );
add_filter( 'query_vars', 'jjcp_custom_query_vars_filter' );
add_action( 'admin_menu', 'jjcp_admin_menus');
add_action( 'admin_post_jjcp_save_options', 'jjcp_save_options');


// SHORTCODES
add_shortcode( 'configurador_pistas', 'configurador_pistas_shortcode' );
