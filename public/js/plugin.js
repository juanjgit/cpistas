function boton2id(boton) {
    // Identidfica el elemento al que se refiere un botón
    index = boton.id.lastIndexOf('-');
    idElemento = boton.id.substr(index+1);
    return idElemento;
};


function selccionaPista(idPista) {
    // Actualiza el valor para el formulario
    jQuery('input#op-pista').val(idPista);
    // Cambia la imagen de la pista
    imgPistaTipo = idPista;
    actualizaImgPista(imgPistaTipo,imgColorEstructura,imgColorCesped,imgColorBaculo);
    // Activa el botón
    jQuery('li.boton-pista').removeClass('boton-activado');
    jQuery('li#boton-pista-'+idPista).addClass('boton-activado');
}

function seleccionaCesped(idColor) {
    // Actualiza el valor para el formulario
    jQuery('input#op-cesped-color').val(idColor);
    // Actualiza la images
    imgColorCesped = idColor;
    actualizaImgPista(imgPistaTipo,imgColorEstructura,imgColorCesped,imgColorBaculo);
    // Activa el botón
    jQuery('li.boton-cesped').removeClass('boton-activado');
    jQuery('li#boton-cesped-'+idColor).addClass('boton-activado');
}

function seleccionaEstructura(idColor) {
    // Actualiza el valor para el formulario
    jQuery('input#op-estructura-color').val(idColor);
    // Actualiza la images
    imgColorEstructura = idColor;
    actualizaImgPista(imgPistaTipo,imgColorEstructura,imgColorCesped,imgColorBaculo);
    // Activa el botón
    jQuery('li.boton-estructura').removeClass('boton-activado');
    jQuery('li#boton-estructura-'+idColor).addClass('boton-activado');
}

function seleccionaBaculo(idColor) {
    // Actualiza el valor para el formulario
    jQuery('input#op-baculo-color').val(idColor);
    // Actualiza la images
    imgColorBaculo = idColor;
    actualizaImgPista(imgPistaTipo,imgColorEstructura,imgColorCesped,imgColorBaculo);
    // Activa el botón
    jQuery('li.boton-baculo').removeClass('boton-activado');
    jQuery('li#boton-baculo-'+idColor).addClass('boton-activado');
}


function validaCampos() {
    // Habilita el botón de finalizar si todas las opciones están seleccionadas
    if ( jQuery('#readonly').val() == 0 ){
        if ( jQuery('input#op-pista').val() != '0' && jQuery('input#op-cesped-color').val() != '0' &&  jQuery('input#op-estructura-color').val() != '0' && jQuery('input#op-baculo-color').val() != '0' )  {
            jQuery('#boton-fin').show();
             jQuery('#pista-status').text("Opciones seleccionadas. Pulse el botón de finalizar.");
        };
    }
}

function actualizaImgPista(imgPistaTipo,imgColorEstructura,imgColorCesped,imgColorBaculo) {
    // Cambia la imagen de la pista
    imagenActual = jQuery('img#imagen-pista').attr("src");
    index = imagenActual.lastIndexOf("/");
    pathImagen = imagenActual.substr(0,index);
    imagenNueva = pathImagen+'/'+'pista'+imgPistaTipo+'_'+imgColorEstructura+'_'+imgColorCesped+'_'+imgColorBaculo+'.jpg';
    // Solo actualiza la imagen si es imagen inicial o si se han seleccionado todas las opciones
    if ( ( imgColorEstructura == 0 && imgColorCesped == 0 && imgColorBaculo == 0 ) || ( imgColorEstructura != 0 && imgColorCesped != 0 && imgColorBaculo != 0 ) )
    {
        jQuery('#imagen-pista').attr('src',imagenNueva);
    } 
    
}

function actualizaDescripcion(imgPistaTipo,imgColorEstructura,imgColorCesped,imgColorBaculo) {
    // Rellena textos de la descripción
    //console.log(my_ajax_obj.textos);
    var txtpista = my_ajax_obj.textos['txt-tipo-pista-'+imgPistaTipo];
    var txcesped = my_ajax_obj.textos['txt-color-pista-'+imgColorCesped];
    var txtestructura = my_ajax_obj.textos['txt-color-estructura-'+imgColorEstructura];
    var txtbaculos = my_ajax_obj.textos['txt-color-baculo-'+imgColorBaculo];
    jQuery("#descripcionresultado").html(jQuery("#descripcionresultado").html().replace('TXTPISTA',txtpista));
    jQuery("#descripcionresultado").html(jQuery("#descripcionresultado").html().replace('TXTCESPED',txcesped));
    jQuery("#descripcionresultado").html(jQuery("#descripcionresultado").html().replace('TXTESTRUCTURA',txtestructura));
    jQuery("#descripcionresultado").html(jQuery("#descripcionresultado").html().replace('TXTBACULOS',txtbaculos));
       
}

jQuery(function($){
    $(document).ready(function(){
        // Inicializa estilos
        $('li.botonera-opciones').removeClass('boton-activado');
        $('li.botonera-opciones').hide();
        $('#boton-fin').hide();
        $('div.wpcf7[role=form]').hide();
        $('#resultado').hide();
        
        // inicializa imagen
        imgPistaTipo = my_ajax_obj.pista;  // Seleccionado como atributo
        imgColorEstructura = 1;
        imgColorCesped = 1;
        imgColorBaculo = 1;
        if ( jQuery('#readonly').val() == 0 ){
            actualizaImgPista(imgPistaTipo,imgColorEstructura,imgColorCesped,imgColorBaculo);
        

            // Si existe un parámetro de pista, inhabilita los botones
            //alert ('imgPistaTipo:'+imgPistaTipo+' jQuery("#readonly").val():'+jQuery('#readonly').val());
            if ( imgPistaTipo != 0 ) {
                selccionaPista(my_ajax_obj.pista);
                $('div#pista ul.botonera-opciones').hide();
            }
        }

        // Selecciona opciones en el visor
        if ( jQuery('#readonly').val() == "1" ){
            imgPistaTipo = $('input#op-pista').val();
            imgColorEstructura = $('input#op-estructura-color').val();
            imgColorCesped = $('input#op-cesped-color').val();
            imgColorBaculo = $('input#op-baculo-color').val();
            actualizaDescripcion(imgPistaTipo,imgColorEstructura,imgColorCesped,imgColorBaculo);
            
        }

        if ( jQuery('#readonly').val() == 0 ){
            // Click en los botones de opciones
            $('li.boton-pista').on({
                "click": function() {selccionaPista(boton2id(this)); validaCampos(); }   
            });    
            $('li.boton-cesped').on({
                "click": function() { seleccionaCesped(boton2id(this)); validaCampos();}
            });    
            $('li.boton-estructura').on({
                "click": function() { seleccionaEstructura(boton2id(this)); validaCampos(); }
            }); 
            $('li.boton-baculo').on({
                "click": function() { seleccionaBaculo(boton2id(this)); validaCampos(); }
            }); 
            
            $('div#url-resultado').hide(); 
         }
        
        
        // Boton FIN
        $("#boton-fin").click(function() {             
            var this2 = this;
            var formulario = {
                referer: window.location.href,
                op_pista: $('#op-pista').val(),
                op_cesped_color: $('#op-cesped-color').val(),
                op_estructura_color: $('#op-estructura-color').val(),
                op_baculo_color: $('#op-baculo-color').val()
            };
                 
            var data = {         //POST request
               _ajax_nonce: my_ajax_obj.nonce,      //nonce
                action: "jjcp_configurar",     //action
                title: formulario   //data
            };
            //use in callback
            $.post(my_ajax_obj.ajax_url, data , function(response) { 
                //callback
                // Introduce la URL a compartir por redes sociales
                var url=response['urlcpistas'];
                //$('#url-resultado').html('Use este link para compartir su configuración: <a href="'+url+'">'+url+'</a>');
                var plantillaredes = $('#url-resultado').html();
                $('#url-resultado').html(plantillaredes.replace(/SHAREURL/g,encodeURIComponent(url)));
                $('#opciones').hide();
                $('#resultado').show();
                $('input[name=url-pista]').val(url);
            });
            
            actualizaDescripcion(imgPistaTipo,imgColorEstructura,imgColorCesped,imgColorBaculo);
            // Inhabilita botones
            jQuery('ul.botonera-opciones').hide();
            
            jQuery('#pista-status').text("");
            //scroll back up
            $("html, body").animate({
                //scrollTop: $("#pista").offset().top
                scrollTop: $("html, body").offset().top
            }, 300);
        });

        // Boton Presupuesto
        $("#boton-presupuesto").click(function() {
            //$('div#configurador-pistas-sc').hide();
            $('div.wpcf7[role=form]').dialog({
                    modal: true,
                    height: "auto",
                    width: "auto",
                    minWidth: 450
                });
        });
        
          // Boton compartir
        $("#boton-compartir").click(function() {
            $('div#url-resultado').show();     
           
        });
      
        
        
       // Boton guardar imagen
       // TODO
    });
});

