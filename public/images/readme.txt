En este directorio deben estar las imágenes con las siguiente nomenclatura:

pista{p}_{c1}_{c2}_{c3}.jpg

Donde:
    p: pista
    c1: color opción 1
    c2: color opción 2
    c3: color opción 3

Ejemplo:
    pista2_7_8_7.jpg

Imagenes iniciales, sin opciones:
    pista{p}_0_0_0.jpg