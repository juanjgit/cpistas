MODELO CLUB10

Color césped: verde, azul, negra, naranja (lo que dijeron)
Color césped: verde, azul, naranja, purpura,  (lo que envían en render) 
Color Estructura: verde, azul, negra, blanca, gris, (lo que dijeron)
Color Estructura: verde, azul, negra, blanca, gris, rosa  (lo que envían en render) 
Baculos Focos: Estandard verdes, Estandard azules, Estandard Negros, Estandard Blancos, Estandard Grises, Tipo Z verdes, Tipo Z Azules, Tipo Z Negros, Tipo Z Blancos,Tipo Z Grises, Tipo C verdes, Tipo C Azules, Tipo C negros, Tipo C Blancos, Tipo C Grises.
Baculos Focos: verde, negro, blanco, gris, rosa 
Puertas: Sin Puertas, Con Puertas.

MODELO PRO10

Color césped: verde, azul, negra, naranja  (lo que dijeron)
Color césped: verde, azul, naranja, purpura (lo que envían en render) 
Color Estructura: verde, azul, negra, blanca, gris,
Color Estructura: verde, azul, negra, blanca, gris, rosa  (lo que envían en render) 
Baculos Focos: Estandard verdes, Estandard azules, Estandard Negros, Estandard Blancos, Estandard Grises, Tipo Z verdes, Tipo Z Azules, Tipo Z Negros, Tipo Z Blancos,Tipo Z Grises, Tipo C verdes, Tipo C Azules, Tipo C negros, Tipo C Blancos, Tipo C Grises.
Puertas: Sin Puertas, Con Puertas.

MODELO TOP10

Color césped: verde, azul, negra, naranja
Color Estructura: verde, azul, negra, blanca, gris, rosa.
Baculos Focos: Estandard verdes, Estandard azules, Estandard Negros, Estandard Blancos, Estandard Grises, Tipo Z verdes, Tipo Z Azules, Tipo Z Negros, Tipo Z Blancos,Tipo Z Grises, Tipo C verdes, Tipo C Azules, Tipo C negros, Tipo C Blancos, Tipo C Grises.
Puertas: Sin Puertas, Con Puertas.

PISTAS:
1: CLUB10
2: PRO10
3: TOP10

COLORES:
1: Verde
2: Azul
3: Negro
4: Naranja
5: Blanco
6: Gris
7: Rosa
8: purpura

BOOLEANO:
0: No
1: Si

rename 's/verde/1/' *
rename 's/azul/2/' *
rename 's/negra/3/' *
rename 's/negro/3/' *
rename 's/negre/3/' *
rename 's/naranja/4/' *
rename 's/blanca/5/' *
rename 's/blanco/5/' *
rename 's/gris/6/' *
rename 's/rosa/7/' *
rename 's/purpura/8/' *

rename 's/^/pista3_/' *

USO:

Añadir al artículo o página el 'shortcode' [configurador_pistas {pista=N}]
Parámetros:
 - pista: opcional. Si no se indica se muestra un selector con las pistas

Es necesario crear un formulario de contacto con el plugin contactform7. 
Debe contener una variable oculta "[hidden url-pista]"

