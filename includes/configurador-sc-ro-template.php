<div id="configurador-pistas-sc">
    <div id="pista-status"></div>
    <form id="cofigurador-form">
        <div id="pista">
            <h2>PISTA_I18N</h2>

            <img id="imagen-pista" src="IMAGE_VOID" alt="pista de padel">
        </div>
       
        <div id="resultado" >
            <div id="boton-fin">FIN_I18N</div>
        </div>
                <div id="resultado" >  
            <h2>TITULO_I18N</h2>
            <ul id="descripcionresultado">
                <li>PISTA_I18N  TXTPISTA</li>
                <li>CESPED_I18N  TXTCESPED</li>
                <li>ESTRUCTURA_I18N  TXTESTRUCTURA</li>
                <li>BACULOS_I18N  TXTBACULOS</li>
            </ul>
            <ul class="botonera-final">
                <li class="cp-boton" id="boton-presupuesto">BOTONPRESUPUESTO_I18N</li>
                <!-- li class="cp-boton" id="boton-guardar">BOTONGUARDAR_I18N</li -->
                <li class="cp-boton" id="boton-compartir">BOTONCOMPARTIR_I18N</li>
            </ul>
            <div id="url-resultado">
                <!-- Twitter -->
                <a href="https://twitter.com/intent/tweet?url=SHAREURL&text=TEXTOCOMPARIR_I18N&via=padel10" title="Share on Twitter" target="_blank" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
                 <!-- Facebook -->
                <a href="https://www.facebook.com/sharer.php?u=SHAREURL" title="Share on Facebook" target="_blank" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                <!-- Google+ -->
                <a href="https://plus.google.com/share?url=SHAREURL" title="Share on Google+" target="_blank" class="btn btn-googleplus"><i class="fa fa-google-plus"></i> Google+</a>
                 <!-- LinkedIn --> 
                <a href="https://www.linkedin.com/shareArticle?url=SHAREURL&title=TEXTOCOMPARIR_I18NL" title="Share on LinkedIn" target="_blank" class="btn btn-linkedin"><i class="fa fa-linkedin"></i> LinkedIn</a>
                 <!-- WhatsApp --> 
                <a href="whatsapp://send?text=SHAREURL" title="Share on Whatsapp" target="_blank" class="btn btn-whatsapp"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                 <!-- Email --> 
                <a href="mailto:?subject=TEXTOCOMPARIREMAIL_I18NL&amp;body=TEXTOCOMPARIREMAIL_I18NL SHAREURL" title="Share on EMail" target="_blank" class="btn btn-email"><i class="fa fa-envelope"></i> Email</a>
     
            </div>
            <div id="nota"><p><small>NOTARESULTADO_I18N</small></p></div>
        </div>
        
        <input id="op-pista" name="op-pista" type="hidden" value="OP_PISTA" />
        <input id="op-cesped-color" name="op-cesped-color" type="hidden" value="OP_CESPED" />
        <input id="op-estructura-color" name="op-estructura-color" type="hidden" value="OP_ESTRUCTURA" />
        <input id="op-baculo-color" name="op-baculo-color" type="hidden" value="OP_BACULO" />
        <input id="readonly" name="readonly" type="hidden" value="1" />
    </form>
</div>