<?php

function jjcp_save_options () {
   /* echo '<pre>';
    print_r( $_POST );
    echo '</pre>';
    die(); */
    if ( !current_user_can('edit_pages')) {
        wp_die('No autorizado');
    }
    check_admin_referer('jjcp_options_verify');
    
    $opts_pista = get_option('cp_opts');
    $opts_pista['txt-tipo-pista-1'] =   sanitize_text_field($_POST['txt-tipo-pista-1']);
    $opts_pista['txt-tipo-pista-2'] =   sanitize_text_field($_POST['txt-tipo-pista-2']);
    $opts_pista['txt-tipo-pista-3'] =   sanitize_text_field($_POST['txt-tipo-pista-3']);
    $opts_pista['txt-color-pista-1'] =   sanitize_text_field($_POST['txt-color-pista-1']);
    $opts_pista['txt-color-pista-2'] =   sanitize_text_field($_POST['txt-color-pista-2']);
    $opts_pista['txt-color-pista-4'] =   sanitize_text_field($_POST['txt-color-pista-4']);
    $opts_pista['txt-color-pista-8'] =   sanitize_text_field($_POST['txt-color-pista-8']);
    $opts_pista['txt-color-estructura-1'] =   sanitize_text_field($_POST['txt-color-estructura-1']);
    $opts_pista['txt-color-estructura-2'] =   sanitize_text_field($_POST['txt-color-estructura-2']);
    $opts_pista['txt-color-estructura-3'] =   sanitize_text_field($_POST['txt-color-estructura-3']);
    $opts_pista['txt-color-estructura-4'] =   sanitize_text_field($_POST['txt-color-estructura-4']);
    $opts_pista['txt-color-estructura-5'] =   sanitize_text_field($_POST['txt-color-estructura-5']);
    $opts_pista['txt-color-estructura-6'] =   sanitize_text_field($_POST['txt-color-estructura-6']);
    $opts_pista['txt-color-estructura-7'] =   sanitize_text_field($_POST['txt-color-estructura-7']);
    $opts_pista['txt-color-estructura-8'] =   sanitize_text_field($_POST['txt-color-estructura-8']);
    $opts_pista['color-baculo-1'] =   sanitize_text_field($_POST['color-baculo-1']);
    $opts_pista['color-baculo-2'] =   sanitize_text_field($_POST['color-baculo-2']);
    $opts_pista['color-baculo-3'] =   sanitize_text_field($_POST['color-baculo-3']);
    $opts_pista['color-baculo-4'] =   sanitize_text_field($_POST['color-baculo-4']);
    $opts_pista['color-baculo-5'] =   sanitize_text_field($_POST['color-baculo-5']);
    $opts_pista['color-baculo-6'] =   sanitize_text_field($_POST['color-baculo-6']);
    $opts_pista['color-baculo-7'] =   sanitize_text_field($_POST['color-baculo-7']);
    $opts_pista['color-baculo-8'] =   sanitize_text_field($_POST['color-baculo-8']);
    
    update_option('cp_opts', $opts_pista);
    wp_redirect(admin_url('admin.php?page=jjcp_plugin_opts&status=1'));
}
