<?php

function jjcp_activate_plgin() {
    /***
     * 
CREATE TABLE `wordpress`.`wp_cpistas` (
  `ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `once` VARCHAR(45) NULL,
  `op_pista` INT UNSIGNED NULL DEFAULT 0,
  `op_cesped_color` INT UNSIGNED NULL DEFAULT 0,
  `op_estructura_color` INT UNSIGNED NULL DEFAULT 0,
  `user_ip` VARCHAR(40) NULL,
  `fecha` DATETIME NULL,
  PRIMARY KEY (`ID`)););

     */
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    
     $createSQL  = "  CREATE TABLE `". $wpdb->prefix ."cpistas` (
        `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `cpistasid` varchar(45) DEFAULT NULL,
        `pista` int(10) unsigned DEFAULT '0',
        `cesped` int(10) unsigned DEFAULT '0',
        `estructura` int(10) unsigned DEFAULT '0',
        `user_ip` varchar(40) DEFAULT NULL,
        `fecha` datetime NOT NULL,
        `baculo` int(10) unsigned DEFAULT '0',
        PRIMARY KEY (`ID`)
    ) " .$charset_collate. ";";
 
    require_once( ABSPATH . '/wp-admin/includes/upgrade.php');
    dbDelta( $createSQL );
    
    $cp_opts = get_option('cp_opts');
    
    if ( !$cp_opts ) {
        $opts_pista = [
            'txt-tipo-pista-1' => 'Pista modelo Club10',
            'txt-tipo-pista-2' => 'Pista modelo Pro10',
            'txt-tipo-pista-3' => 'Pista modelo Top10',
            'txt-color-pista-1' => 'Pista verde',
            'txt-color-pista-2' => 'Pista azul',
            'txt-color-pista-4' => 'Pista naranja',
            'txt-color-pista-8' => 'Pista púrpura',
            'txt-color-estructura-1' => 'Estructura verde',
            'txt-color-estructura-2' => 'Estructura azul',
            'txt-color-estructura-3' => 'Estructura negra',
            'txt-color-estructura-4' => 'Estructura naranja',
            'txt-color-estructura-5' => 'Estructura blanca',
            'txt-color-estructura-6' => 'Estructura gris',
            'txt-color-estructura-7' => 'Estructura rosa',
            'txt-color-estructura-8' => 'Estructura púrpura',
            'txt-color-baculo-1' => 'Red y postes verdes',
            'txt-color-baculo-2' => 'Red y postes azules',
            'txt-color-baculo-3' => 'Red y postes negros',
            'txt-color-baculo-4' => 'Red y postes naranjas',
            'txt-color-baculo-5' => 'Red y postes blancos',
            'txt-color-baculo-6' => 'Red y postes grises',
            'txt-color-baculo-7' => 'Red y postes rosas',
            'txt-color-baculo-8' => 'Red y postes púrpuras'
            ];
       
        add_option('cp_opts', $opts_pista);
    }
}

