<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function jjcp_plugin_opts_page() {
    $opts_pista = get_option('cp_opts');
    ?>
<div class="wrap">
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title"><?php _e('Textos configurador', 'cpistas' ); ?></h3>
    </div>
    <?php
    
    if (isset($_GET['status']) && $_GET['status'] == 1) {
        ?><div class="alert alert-success">Actualización correcta</div><?php
    }
    
    ?>
    <div class="panel-body">
        <form method="POST" action="admin-post.php">
            <input type="hidden" name="action" value="jjcp_save_options">
            <?php wp_nonce_field('jjcp_options_verify') ?>
            <div class="form-group">
                <label>txt-tipo-pista-1</label><input type="text" name="txt-tipo-pista-1" value="<?php echo $opts_pista['txt-tipo-pista-1'] ?>"> <br>
                <label>txt-tipo-pista-2</label><input type="text" name="txt-tipo-pista-2" value="<?php echo $opts_pista['txt-tipo-pista-2'] ?>"> <br>
                <label>txt-tipo-pista-3</label><input type="text" name="txt-tipo-pista-3" value="<?php echo $opts_pista['txt-tipo-pista-3'] ?>"> <br>
                <label>txt-color-pista-1</label><input type="text" name="txt-color-pista-1" value="<?php echo $opts_pista['txt-color-pista-1'] ?>"> <br>
                <label>txt-color-pista-2</label><input type="text" name="txt-color-pista-2" value="<?php echo $opts_pista['txt-color-pista-2'] ?>"> <br>
                <label>txt-color-pista-4</label><input type="text" name="txt-color-pista-4" value="<?php echo $opts_pista['txt-color-pista-4'] ?>"> <br>
                <label>txt-color-pista-8</label><input type="text" name="txt-color-pista-8" value="<?php echo $opts_pista['txt-color-pista-8'] ?>"> <br>
                <label>txt-color-estructura-1</label><input type="text" name="txt-color-estructura-1" value="<?php echo $opts_pista['txt-color-estructura-1'] ?>"> <br>
                <label>txt-color-estructura-2</label><input type="text" name="txt-color-estructura-2" value="<?php echo $opts_pista['txt-color-estructura-2'] ?>"> <br>
                <label>txt-color-estructura-3</label><input type="text" name="txt-color-estructura-3" value="<?php echo $opts_pista['txt-color-estructura-3'] ?>"> <br>
                <label>txt-color-estructura-4</label><input type="text" name="txt-color-estructura-4" value="<?php echo $opts_pista['txt-color-estructura-4'] ?>"> <br>
                <label>txt-color-estructura-5</label><input type="text" name="txt-color-estructura-5" value="<?php echo $opts_pista['txt-color-estructura-5'] ?>"> <br>
                <label>txt-color-estructura-6</label><input type="text" name="txt-color-estructura-6" value="<?php echo $opts_pista['txt-color-estructura-6'] ?>"> <br>
                <label>txt-color-estructura-7</label><input type="text" name="txt-color-estructura-7" value="<?php echo $opts_pista['txt-color-estructura-7'] ?>"> <br>
                <label>txt-color-estructura-8</label><input type="text" name="txt-color-estructura-8" value="<?php echo $opts_pista['txt-color-estructura-8'] ?>"> <br>
                <label>txt-color-baculo-1</label><input type="text" name="txt-color-baculo-1" value="<?php echo $opts_pista['txt-color-baculo-1'] ?>"> <br>
                <label>txt-color-baculo-2</label><input type="text" name="txt-color-baculo-2" value="<?php echo $opts_pista['txt-color-baculo-2'] ?>"> <br>
                <label>txt-color-baculo-3</label><input type="text" name="txt-color-baculo-3" value="<?php echo $opts_pista['txt-color-baculo-3'] ?>"> <br>
                <label>txt-color-baculo-4</label><input type="text" name="txt-color-baculo-4" value="<?php echo $opts_pista['txt-color-baculo-4'] ?>"> <br>
                <label>txt-color-baculo-5</label><input type="text" name="txt-color-baculo-5" value="<?php echo $opts_pista['txt-color-baculo-5'] ?>"> <br>
                <label>txt-color-baculo-6</label><input type="text" name="txt-color-baculo-6" value="<?php echo $opts_pista['txt-color-baculo-6'] ?>"> <br>
                <label>txt-color-baculo-7</label><input type="text" name="txt-color-baculo-7" value="<?php echo $opts_pista['txt-color-baculo-7'] ?>"> <br>
                <label>txt-color-baculo-8</label><input type="text" name="txt-color-baculo-8" value="<?php echo $opts_pista['txt-color-baculo-8'] ?>"> <br> 
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary"><?php _e('Actualizar', 'cpistas'); ?></button>
            </div>
        </form>
    </div>
</div>
</div>

    <?php
}
