<?php

function jjcp_admin_enqueue () {
    
    if ( !isset($_GET['page']) || $_GET['page'] != "jjcp_plugin_opts") {
    return;
    }

    wp_register_style ( 'ju_bootstrap', plugins_url ( '/assets/styles/bootstrap.css', CPISTAS_PLUGIN_URL ));
    wp_enqueue_style ('ju_bootstrap');
    
    wp_enqueue_scripts();
}


