<?php

function jjcp_ajax_configurar()
{
    // Handle the ajax request
   check_ajax_referer( 'jjcp_configurador-nonce' );
    
    global $wpdb;
    
    $datos = $_POST['title'];
    $referer = $datos['referer'];
    $pista = $datos['op_pista'];
    $cesped_color = $datos['op_cesped_color'];
    $estrcuctura_color = $datos['op_estructura_color'];
    $baculo_color = $datos['op_baculo_color'];
    $user_IP = $_SERVER['REMOTE_ADDR'];
    
    
    
    $params = array(
        'cpistasid' => uniqid(),
        'pista' => $pista,
        'cesped'=> $cesped_color,
        'estructura' => $estrcuctura_color,
        'baculo' => $baculo_color,
        'user_ip' => $user_IP,
        'fecha' => current_time( 'mysql' )  
     );
    
    $wpdb->insert (
            $wpdb->prefix . 'cpistas',
            $params,
            array('%s','%d','%d','%d','%d','%s','%s')
            );
    
    $url = add_query_arg('cpistasid',$params['cpistasid'],$referer);
    
    $respuesta = array(
        'status'    =>  1,
        'urlcpistas'   =>  $url
    );
    
    wp_send_json( $respuesta );

    wp_die(); // All ajax handlers die when finished
}
