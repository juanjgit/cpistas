<?php

function configurador_pistas_shortcode($atts = []) {
    
    // Atributos pasados al shortcode
    $attr_sc = shortcode_atts(
            array(
                'pista' => '0'
            ),
            $atts);
    $attr_npista = $attr_sc['pista'];
    
    // Carga de variables y arma el javascript
    $title_nonce = wp_create_nonce( 'jjcp_configurador-nonce' );
    $opts_pista = get_option('cp_opts');
    
    wp_localize_script( 'cpistas_script', 'my_ajax_obj', array(
          'ajax_url' => admin_url( 'admin-ajax.php' ),
          'nonce'    => $title_nonce, 
          'pista' => $attr_npista,
          'textos' => $opts_pista
    )); 
    wp_enqueue_script( 'cpistas_script' );
       
    // Carga el configurador o el visor de pistas
    $cpistasid = get_query_var('cpistasid');
    if ($cpistasid != '') {
        $configuradorHTML   = file_get_contents('configurador-sc-ro-template.php', true);
    }
    else {
        $configuradorHTML   = file_get_contents('configurador-sc-template.php', true);
    }
   
    // Textos
    $configuradorHTML = str_replace("TITULO_I18N", __("CONFIGURADOR PADEL 10", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("SELECCIONE_I18N", __("Seleccione las opciones deseadas", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("PISTA_I18N", __("Pista", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("OPCIONES_I18N", __("Opciones", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("COLOREST_I18N", __("Color Estructura", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("COLORCESPED_I18N", __("Color Césped", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("COLORBACULO_I18N", __("Color Red y focos", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("BACULOSFOCOS_I18N", __("Báculos Focos", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("TIPO_I18N", __("Tipo", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("COLOR_I18N", __("Color", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("OPUERTAS_I18N", __("Puertas", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("FIN_I18N", __("Finalizar", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("NPISTA1_I18N", __("Club10", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("NPISTA2_I18N", __("Pro10", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("NPISTA3_I18N", __("Top10", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("NPISTA3_I18N", __("Top10", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("TBACULO1_I18N", __("Estándar", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("TBACULO2_I18N", __("Tipo Z", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("TBACULO3_I18N", __("Tipo C", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("VERDE_I18N", __("Verde", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("AZUL_I18N", __("Azul", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("NEGRO_I18N", __("Negro", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("PURPURA_I18N", __("Púrpura", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("NARANJA_I18N", __("Naranja", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("BLANCO_I18N", __("Blanco", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("GRIS_I18N", __("Gris", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("ROSA_I18N", __("Rosa", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("SPUERTAS_I18N", __("Sin Puertas", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("CPUERTAS_I18N", __("Con Puertas", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("BOTONPRESUPUESTO_I18N", __("Solicitar Presupuesto", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("BOTONGUARDAR_I18N", __("Guardar Imagen", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("BOTONCOMPARTIR_I18N", __("Compartir", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("NOTARESULTADO_I18N", __("nota: esto es un ejemplo de colores, en el caso de la estructura disponemos de más colores<br>También se pueden hacer otras formas y colores para los báculos de los focos", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("TEXTOCOMPARIR_I18N", __("Esta%20es%20la%20pista%20que%20me%20gusta%20de%20PADEL10.COM", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("TEXTOCOMPARIREMAIL_I18NL", __("Esta es la pista que me gusta de PADEL10.COM", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("PISTA_I18N", __("Pista:", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("CESPED_I18N", __("Césped:", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("ESTRUCTURA_I18N", __("Estructura:", "cpistas") ,$configuradorHTML);
    $configuradorHTML = str_replace("BACULOS_I18N", __("Báculos:", "cpistas") ,$configuradorHTML);


    // Valores predeterminados para el visor de pistas
    if (get_query_var('cpistasid') != '') {
        global $wpdb;
        $query = "SELECT * FROM ". $wpdb->prefix ."cpistas WHERE cpistasid='".$cpistasid."'";
        $pista = $wpdb->get_row($query, ARRAY_A);
        
        $configuradorHTML = str_replace("OP_PISTA", $pista['pista'] ,$configuradorHTML);
        $configuradorHTML = str_replace("OP_CESPED", $pista['cesped'] ,$configuradorHTML);
        $configuradorHTML = str_replace("OP_ESTRUCTURA", $pista['estructura'] ,$configuradorHTML);
        $configuradorHTML = str_replace("OP_BACULO", $pista['baculo'] ,$configuradorHTML);
        
    }
    
    // Imagen inicial
    if ($cpistasid != '') {
        $imagevoid = plugins_url( 'public/images/', CPISTAS_PLUGIN_URL )."pista".$pista['pista']."_".$pista['estructura']."_".$pista['cesped']."_".$pista['baculo'].".jpg";

    }
    else {
        $imagevoid = plugins_url('public/images/pista'.$attr_npista.'_0_0_0.jpg',CPISTAS_PLUGIN_URL);  

    }
    $configuradorHTML = str_replace("IMAGE_VOID",$imagevoid,$configuradorHTML);
        
    return $configuradorHTML;
}

function jjcp_custom_query_vars_filter($vars) {
    $vars[] = 'cpistasid';
    $vars[] = 'cesped';
    $vars[] = 'estructura';
    $vars[] = 'baculo';
    return $vars;
}

