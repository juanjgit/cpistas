<?php

function jjcp_register_plugin_styles() {
    global $post;
    // Solo carga estilos y js si esta presente el shortcode
    if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'configurador_pistas') ) {
        wp_register_style( 'cpistas', plugins_url( 'cpistas/public/css/plugin.css' ) );
	wp_enqueue_style( 'cpistas' );
        
        wp_enqueue_script( 'jquery-ui-dialog' );
        wp_enqueue_style( 'wp-jquery-ui-dialog' );
        
        wp_register_script( 'cpistas_script', plugins_url('cpistas/public/js/plugin.js'), array('jquery'));            
    
        //wp_register_script( 'FontAwesome', 'https://use.fontawesome.com/releases/v5.0.8/js/all.js', null, null, true );
        //wp_enqueue_script('FontAwesome');
        
        
    }       

}


